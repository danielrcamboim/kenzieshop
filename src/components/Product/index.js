import { useDispatch } from "react-redux"
import { addCartThunk, removeCartThunk } from "../../store/modules/cart/thunks"
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles({
    root: {
      maxWidth: 345,
      width: 330,
      height: 350,
      borderRadius: 10,
      margin: 10,
    },
    media: {
      height: 180,
    },
    typography: {
        fontFamily: "Roboto Mono, monospace",
        textAlign: "center",
    }
  });

const Product = ({product, isRemovable = false}) => {
    
    const classes = useStyles();
    const dispatch = useDispatch()

    const { id, name, price, image } = product
    
    return(
        <Card className={classes.root} variant="outlined">
            <CardActionArea>
                <CardMedia
                className={classes.media}
                image={image}
                title={name}
                />
            <CardContent>
                <Typography gutterBottom variant="h5" component="h2" className={classes.typography}>
                    {name}
                </Typography>
                <Typography variant="body2" color="textSecondary" component="p" className={classes.typography}>
                    $ {price}
                </Typography>
            </CardContent>
            </CardActionArea>
            <CardActions style={{justifyContent: 'center'}} >
                
                { isRemovable ? 
                    (<Button size="small" color="primary" onClick={() => dispatch(removeCartThunk(id)) }>Remove</Button>)
                    :
                    (<Button size="small" color="primary" onClick={() => dispatch(addCartThunk(product)) }>Add to cart</Button>)
                }
            </CardActions>
        </Card>
    )
}

export default Product
