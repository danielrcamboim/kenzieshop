import React from "react"
import { makeStyles } from "@material-ui/core/styles"
import AppBar from "@material-ui/core/AppBar"
import Typography from "@material-ui/core/Typography"
import Toolbar from "@material-ui/core/Toolbar"
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import Badge from '@material-ui/core/Badge'
import IconButton from '@material-ui/core/IconButton'
import HomeIcon from '@material-ui/icons/Home';
import { useSelector } from "react-redux"
import { Link } from "react-router-dom"

const useStyles = makeStyles((theme) => ({
    bar: {
        backgroundColor: "#b06130",
    },
    toolbar: {
        justifyContent: "space-between"
    },
    title: {
        fontWeight: "bold",
        fontSize: "32px",
        fontFamily: "Itim, cursive",
        color: "#080c09"
    },
    icon: {
        fontSize: "25px",
        color: "white",
    }
}))

const Header = () => {
    
    const classes = useStyles()

    const cart = useSelector((store) => store.cart)
    
    return (
        <AppBar position="static" variant="outlined" className={classes.bar}>
            <Toolbar className={classes.toolbar}>
                <Typography className={classes.title}>KenziePlantsShop</Typography>
                <div>
                    <Link to="/">
                        <IconButton>
                            <HomeIcon className={classes.icon}/>
                        </IconButton>
                    </Link>
                    <Link to="/cart">
                        <IconButton>
                            <Badge badgeContent={cart.length} color="primary"> 
                            <   ShoppingCartIcon className={classes.icon}/>
                            </Badge>
                        </IconButton>
                    </Link>
                </div>
            </Toolbar>
        </AppBar>
    )
}

export default Header