import { useSelector } from "react-redux"
import Product from "../Product"

const Products = () => {
    
    const products = useSelector((store) => store.products)

    return(
        <>
            { products.map((product) => (<Product key={product.id} product={product} />)) }
        </>
    )
}

export default Products