import { useSelector } from "react-redux"
import Product from "../Product"

const Cart = () => {
    
    const cart = useSelector((store) => store.cart)
    
    return(
        <div className="cart">
            <div>
                <h1>Cart</h1>
                <h2>Total - </h2>
                <h2>{cart.reduce((acc, product) => acc + product.price, 0)}</h2>
            </div>
            <div className="cartProducts">    
                {cart.map((product) => (
                    <Product key={product.id} product={product} isRemovable />
                ))}
            </div>
        </div>
    )
}

export default Cart