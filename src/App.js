import './App.css';

import CartPage from './components/Cart';
import Home from "../src/pages/Home"
import Header from './components/Header';
import { Switch, Route } from 'react-router-dom';

function App() {
  return (
    <div className="App">
      <Header></Header>
        <div className="content">
          <Switch>
            <Route exact path="/">
              <Home/>
            </Route>
            <Route path="/cart">
              <CartPage/>
            </Route>
          </Switch>
        </div>
    </div>
  );
}

export default App;
