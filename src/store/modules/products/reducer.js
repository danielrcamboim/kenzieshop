const defaultProducts = [
	{id: 1, name: "Aglaonema Butterfly", price: 25.00, image: "https://cdn.shopify.com/s/files/1/1881/4693/products/20190213-20190213-P1390087_800x.jpg?v=1550184826"},
	{id: 2, name: "Aglaonema Maria", price: 15.00, image: "https://cdn.shopify.com/s/files/1/1881/4693/products/20180705-20180705-P1290958_800x.jpg?v=1531522079"},
	{id: 3, name: "Aspidistra Cast Iron", price: 60.00, image: "https://cdn.shopify.com/s/files/1/1881/4693/products/20180523-20180523-P1060667-Edit_grande_89f1709b-2a12-4db2-851c-c9e0c12a7dd0_600x.jpg?v=1558740839"},
	{id: 4, name: "Calathea Beauty Star", price: 30.00, image: "https://cdn.shopify.com/s/files/1/1881/4693/products/20200426-20200426-P1510076_800x.jpg?v=1588390378"},
	{id: 5, name: "Cissus Rotundifolia", price: 27.00, image: "https://cdn.shopify.com/s/files/1/1881/4693/products/20181022-20181022-P1330871_800x.jpg?v=1584634433"},
	{id: 6, name: "Anthurium Flamingo", price: 30.00, image: "https://cdn.shopify.com/s/files/1/1881/4693/products/20200404-20200404-P1500769_800x.jpg?v=1588389053"},
]

const productsReducer = (state = defaultProducts, action) => {
    return state
}

export default productsReducer